﻿Uses
  GraphABC;
  
Const
  Buttons = 9; // Количество всех кнопок
  SizePlayer = 20; // Размер игрока (в пикселях)
  PlayerStep = 5; // Скорость игрока (в пикселях пикс/кадр)

Type button = record
  massButtonsProc: array [1..Buttons] of procedure;
  massButtonsX: array [1..Buttons] of Integer;
  massButtonsY: array [1..Buttons] of Integer;
  massButtonsWidth: array [1..Buttons] of Integer;
  massButtonsHeight: array [1..Buttons] of Integer;
  massButtonsActive: array [1..Buttons] of Boolean;
  massButtonsText: array[1..Buttons] of String;
  massButtonsBackground: array[1..Buttons] of Color;
  massButtonsForeground: array[1..Buttons] of Color;
End;

Var
  massButtons: button; // Массив с кнопками
  Running, PrimaryScreen, Paused, Settings: Boolean;
  PlayerX, PlayerY, maxWallUnits, Width, Height: Integer;
  ParamsSettings: array of String;
  Difficult: String;
  CountSetting: Byte;
  FileSettings: Text;

procedure InitSettings();
  Begin
    Difficult := ParamsSettings[0];
    Width := ParamsSettings[1].ToInteger;
    Height := ParamsSettings[2].ToInteger;
    SetWindowSize(Width, Height);
    Window.Title := ParamsSettings[3];
  End;
  
procedure ButClick1();
  Begin
    Running := true;
    PrimaryScreen := true;
    massButtons.massButtonsActive[1] := false;
    massButtons.massButtonsActive[2] := false;
    massButtons.massButtonsActive[5] := false;
    PlayerX := Width div 2 - SizePlayer div 2;
    PlayerY := Height div 2 - SizePlayer div 2;
    maxWallUnits := Height div SizePlayer;
  End;
  
procedure ButClick2();
  Begin
    Halt();
  End;
  
procedure ButClick3();
  Begin
    Running := true;
    Paused := false;
    massButtons.massButtonsActive[3] := false;
    massButtons.massButtonsActive[4] := false;
  End;
  
procedure ButClick4();
  Begin
    Paused := false;
    PrimaryScreen := false;
    massButtons.massButtonsActive[1] := true;
    massButtons.massButtonsActive[2] := true;
    massButtons.massButtonsActive[5] := true;
    massButtons.massButtonsActive[3] := false;
    massButtons.massButtonsActive[4] := false;
  End;
  
procedure ButClick5();
  Begin
    Settings := true;
    PrimaryScreen := true;
    massButtons.massButtonsActive[1] := false;
    massButtons.massButtonsActive[2] := false;
    massButtons.massButtonsActive[5] := false;
    massButtons.massButtonsActive[6] := true;
    massButtons.massButtonsActive[7] := true;
    massButtons.massButtonsActive[8] := true;
    massButtons.massButtonsActive[9] := true;
  End;
  
procedure ButClick6();
  Begin
    Settings := false;
    PrimaryScreen := false;
    massButtons.massButtonsActive[1] := true;
    massButtons.massButtonsActive[2] := true;
    massButtons.massButtonsActive[5] := true;
    massButtons.massButtonsActive[6] := false;
    massButtons.massButtonsActive[7] := false;
    massButtons.massButtonsActive[8] := false;
    massButtons.massButtonsActive[9] := false;
    Difficult := ParamsSettings[0];
    Case Difficult of
      'easy':
      Begin
        massButtons.massButtonsText[8] := 'легко';
        massButtons.massButtonsForeground[8] := clGreen;
      End;
      'normal':
      Begin
        massButtons.massButtonsText[8] := 'нормально';
        massButtons.massButtonsForeground[8] := rgb(222, 222, 0);
      End;
      'hard':
      Begin
        massButtons.massButtonsText[8] := 'сложно';
        massButtons.massButtonsForeground[8] := clRed;
      End;
    End;
  End;
  
procedure ButClick7();
  Begin
  End;
  
procedure ButClick8();
  Begin
    Case Difficult of
      'easy': 
      Begin
        Difficult := 'normal';
        massButtons.massButtonsText[8] := 'нормально';
        massButtons.massButtonsForeground[8] := rgb(222, 222, 0);
      End;
      'normal': 
      Begin
        Difficult := 'hard';
        massButtons.massButtonsText[8] := 'сложно';
        massButtons.massButtonsForeground[8] := clRed;
      End;
      'hard': 
      Begin
        Difficult := 'easy';
        massButtons.massButtonsText[8] := 'легко';
        massButtons.massButtonsForeground[8] := clGreen;
      End;
    End;
  End;
  
procedure ButClick9();
  Begin
    Settings := false;
    PrimaryScreen := false;
    massButtons.massButtonsActive[1] := true;
    massButtons.massButtonsActive[2] := true;
    massButtons.massButtonsActive[5] := true;
    massButtons.massButtonsActive[6] := false;
    massButtons.massButtonsActive[7] := false;
    massButtons.massButtonsActive[8] := false;
    massButtons.massButtonsActive[9] := false;
    Rewrite(FileSettings);
    WriteLn(FileSettings, Difficult);
    WriteLn(FileSettings, Width);
    WriteLn(FileSettings, Height);
    WriteLn(FileSettings, Window.Title);
    Close(FileSettings);
  End;
  
procedure initMass();
  Begin
    massButtons.massButtonsActive[1] := true; // Активна ли кнопна под номером 1
    massButtons.massButtonsWidth[1] := 48; // Ширина кноппи под номером 1
    massButtons.massButtonsHeight[1] := 21; // Высота кнопки под номером 1
    massButtons.massButtonsX[1] := Width div 2 - massButtons.massButtonsWidth[1] div 2; // Положение кнопки 1 по координате X
    massButtons.massButtonsY[1] := Height div 2 - massButtons.massButtonsHeight[1] div 2; // Положение кнопки 1 по координате Y
    massButtons.massButtonsProc[1] := ButClick1; // Процедура которая выполняется при нажатии на кнопку 1
    massButtons.massButtonsText[1] := 'Играть!'; // Текст кнопки 1
    massButtons.massButtonsForeground[1] := clGreen; // Цвет шрифта для кнопки 1 
    massButtons.massButtonsBackground[1] := clWhite; // Цвет заднего фона для кнопки 1
    massButtons.massButtonsActive[2] := true;
    massButtons.massButtonsWidth[2] := 45;
    massButtons.massButtonsHeight[2] := 21;
    massButtons.massButtonsX[2] := Width - massButtons.massButtonsWidth[2];
    massButtons.massButtonsY[2] := Height - massButtons.massButtonsHeight[2];
    massButtons.massButtonsProc[2] := ButClick2;
    massButtons.massButtonsText[2] := 'Выход';
    massButtons.massButtonsForeground[2] := clRed; 
    massButtons.massButtonsBackground[2] := clWhite;
    massButtons.massButtonsActive[3] := false;
    massButtons.massButtonsWidth[3] := 79;
    massButtons.massButtonsHeight[3] := 21;
    massButtons.massButtonsX[3] := Width div 2 - massButtons.massButtonsWidth[3] div 2;
    massButtons.massButtonsY[3] := Height div 2 - massButtons.massButtonsHeight[3];
    massButtons.massButtonsProc[3] := ButClick3;
    massButtons.massButtonsText[3] := 'Продолжить';
    massButtons.massButtonsForeground[3] := clGreen; 
    massButtons.massButtonsBackground[3] := clWhite;
    massButtons.massButtonsActive[4] := false;
    massButtons.massButtonsWidth[4] := 103;
    massButtons.massButtonsHeight[4] := 21;
    massButtons.massButtonsX[4] := Width div 2 - massButtons.massButtonsWidth[4] div 2;
    massButtons.massButtonsY[4] := Height div 2 + 5;
    massButtons.massButtonsProc[4] := ButClick4;
    massButtons.massButtonsText[4] := 'В главное меню';
    massButtons.massButtonsForeground[4] := clRed; 
    massButtons.massButtonsBackground[4] := clWhite;
    massButtons.massButtonsActive[5] := true;
    massButtons.massButtonsWidth[5] := 70;
    massButtons.massButtonsHeight[5] := 21;
    massButtons.massButtonsX[5] := Width div 2 - massButtons.massButtonsWidth[5] div 2;
    massButtons.massButtonsY[5] := Height div 2 + massButtons.massButtonsHeight[5] div 2 + 10;
    massButtons.massButtonsProc[5] := ButClick5;
    massButtons.massButtonsText[5] := 'Настройки';
    massButtons.massButtonsForeground[5] := rgb(222, 222, 0); 
    massButtons.massButtonsBackground[5] := clWhite;
    massButtons.massButtonsActive[6] := false;
    massButtons.massButtonsWidth[6] := 40;
    massButtons.massButtonsHeight[6] := 21;
    massButtons.massButtonsX[6] := Width - massButtons.massButtonsWidth[6];
    massButtons.massButtonsY[6] := Height - massButtons.massButtonsHeight[6];
    massButtons.massButtonsProc[6] := ButClick6;
    massButtons.massButtonsText[6] := 'Назад';
    massButtons.massButtonsForeground[6] := clRed; 
    massButtons.massButtonsBackground[6] := clWhite;
    massButtons.massButtonsActive[7] := false;
    massButtons.massButtonsWidth[7] := 77;
    massButtons.massButtonsHeight[7] := 21;
    massButtons.massButtonsX[7] := Width div 2 - massButtons.massButtonsWidth[7] - 10;
    massButtons.massButtonsY[7] := Height div 2 - massButtons.massButtonsHeight[7] div 2 - 80;
    massButtons.massButtonsProc[7] := ButClick7;
    massButtons.massButtonsText[7] := 'Сложность: ';
    massButtons.massButtonsForeground[7] := clGreen; 
    massButtons.massButtonsBackground[7] := clWhite;
    massButtons.massButtonsActive[8] := false;
    massButtons.massButtonsWidth[8] := 48;
    massButtons.massButtonsHeight[8] := 21;
    massButtons.massButtonsX[8] := Width div 2 + massButtons.massButtonsWidth[8] - 50;
    massButtons.massButtonsY[8] := Height div 2 - massButtons.massButtonsHeight[7] div 2 - 80;
    massButtons.massButtonsProc[8] := ButClick8;
    Case Difficult of
      'easy':
      Begin
        massButtons.massButtonsText[8] := 'легко';
        massButtons.massButtonsForeground[8] := clGreen;
      End;
      'normal':
      Begin
        massButtons.massButtonsText[8] := 'нормально';
        massButtons.massButtonsForeground[8] := rgb(222, 222, 0);
      End;
      'hard':
      Begin
        massButtons.massButtonsText[8] := 'сложно';
        massButtons.massButtonsForeground[8] := clRed;
      End;
    End;
    massButtons.massButtonsBackground[8] := clWhite;
    massButtons.massButtonsActive[9] := false;
    massButtons.massButtonsWidth[9] := 70;
    massButtons.massButtonsHeight[9] := 21;
    massButtons.massButtonsX[9] := Width - massButtons.massButtonsWidth[9];
    massButtons.massButtonsY[9] := Height - massButtons.massButtonsHeight[9] * 2 - 1;
    massButtons.massButtonsProc[9] := ButClick9;
    massButtons.massButtonsText[9] := 'Сохранить';
    massButtons.massButtonsForeground[9] := clGreen; 
    massButtons.massButtonsBackground[9] := clWhite;
  End;

procedure drawButtons();
  Begin
    For var i := 1 to Buttons do
    Begin
      If massButtons.massButtonsActive[i] then
      Begin
        SetBrushColor(massButtons.massButtonsBackground[i]);
        SetFontColor(massButtons.massButtonsForeground[i]);
        FillRectangle(massButtons.massButtonsX[i], massButtons.massButtonsY[i], massButtons.massButtonsX[i] + massButtons.massButtonsWidth[i], massButtons.massButtonsY[i] + massButtons.massButtonsHeight[i]);
        TextOut(massButtons.massButtonsX[i] + 1, massButtons.massButtonsY[i] + 1, massButtons.massButtonsText[i]);
      End;
    End;
    SetBrushColor(clWhite);
    SetFontColor(clBlack);
  End;

procedure MouseDown(X, Y, MB: Integer);
  Begin
    If MB = 1 then
    Begin
      For var i := 1 to Buttons do If (X >= massButtons.massButtonsX[i]) and (X <= massButtons.massButtonsX[i] + massButtons.massButtonsWidth[i]) and (Y >= massButtons.massButtonsY[i]) and (Y <= massButtons.massButtonsY[i] + massButtons.massButtonsHeight[i]) and (massButtons.massButtonsActive[i]) then massButtons.massButtonsProc[i];
    End;
  End;
  
procedure KeyDown(key: Integer);
  Begin
    If (Running) and (not Paused) then
    Begin
      Case key of
        87:
        Begin
          PlayerY -= PlayerStep;
          If PlayerY < 0 then PlayerY := 0;
        End;
        83:
        Begin
          PlayerY += PlayerStep;
          If PlayerY + SizePlayer > Height then PlayerY -= PlayerStep;
        End;
        65:
        Begin
          PlayerX -= PlayerStep;
          If PlayerX < 0 then PlayerX := 0;
        End;
        68:
        Begin
          PlayerX += PlayerStep;
          If PlayerX + SizePlayer > Width then PlayerX -= PlayerStep;
        End;
        27:
        Begin
          Paused := true;
          Running := false;
        End;
      End;
    End;
  End;

Begin
  CountSetting := 1;
  Assign(FileSettings, 'Settings.game');
  Reset(FileSettings);
  while not FileSettings.Eof do
  Begin
    SetLength(ParamsSettings, CountSetting);
    ReadLn(FileSettings, ParamsSettings[CountSetting - 1]);
    inc(CountSetting);
  End;
  FileSettings.Close;
  InitSettings;
  Window.IsFixedSize := true;
  OnMouseDown := MouseDown;
  OnKeyDown := KeyDown;
  initMass;
  While true do
  Begin
    While not PrimaryScreen do
    Begin
      LockDrawing;
      SetBrushColor(clWhite);
      Window.Clear;
      drawButtons;
      Redraw;
    End;
    While Settings do
    Begin
      lockDrawing;
      Window.Clear();
      drawButtons;
      Redraw;
    End;
    While Running do
    Begin
      SetBrushColor(clRed);
      LockDrawing;
      Window.Clear();
      FillRectangle(PlayerX, PlayerY, PlayerX + SizePlayer, PlayerY + SizePlayer);
      Redraw;
    End;
    While Paused do
    Begin
      SetBrushColor(clWhite);
      lockDrawing;
      drawButtons;
      massButtons.massButtonsActive[3] := true;
      massButtons.massButtonsActive[4] := true;
      Redraw;
    End;
  End;
End.